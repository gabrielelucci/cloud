#!/bin/bash

# This script is intended to be run from the appropriate compose project directory.
# It performs some basic maintenance and cleanup tasks that are not run automatically.

PROJECT_DIR=$(pwd)

# Load the compose-utils.sh script.
cd "$( dirname -- "$0"; )";
source ../compose-utils.sh

# --- Additional Compose configuration ---
# COMPOSE_SERVICE: The service to run the command on (check compose yaml file)
# COMPOSE_ARGS: Arguments to pass to the compose command
COMPOSE_SERVICE=${COMPOSE_SERVICE:-"app"}
COMPOSE_ARGS="run --rm ${COMPOSE_SERVICE}"

# Append some args to the compose-utils.sh compose function
alias compose-run="compose ${COMPOSE_ARGS}"

# Reset to the appropriate compose project directory.
cd ${PROJECT_DIR}

# --- Script configuration ---
# TOOTCTL_CONCURRENCY: The number of concurrent workers to use for tootctl commands.
#   Setting this number too high could sometimes flood Object Storage APIs and "soft-DDoS" some instances.
# SEARCH_BATCH_SIZE: The number of records to process in a single batch when rebuilding search indexes.
# MEDIA_REMOVE_DAYS: The number of days after which cached media files are considered stale and removed.
# MEDIA_REFRESH_DAYS: The number of days after which media files to be refreshed.
TOOTCTL_CONCURRENCY=${TOOTCTL_CONCURRENCY:-12}
SEARCH_BATCH_SIZE=${SEARCH_BATCH_SIZE:-200}
MEDIA_REMOVE_DAYS=${MEDIA_REMOVE_DAYS:-60}
MEDIA_REFRESH_DAYS=${MEDIA_REFRESH_DAYS:-60}

# Rebuild ES indexes and clean up unused ones
compose-run tootctl search deploy --batch-size=${SEARCH_BATCH_SIZE} --clean --concurrency=${TOOTCTL_CONCURRENCY}

# Remove cached media files (located under cache/media_attachments) that are older than $MEDIA_REMOVE_DAYS days
compose-run tootctl media remove --days=${MEDIA_REMOVE_DAYS} --concurrency=${TOOTCTL_CONCURRENCY}

# --- DO NOT RUN THESE COMMANDS ---
# See: https://github.com/mastodon/mastodon/issues/23983
#compose-run tootctl media remove --days=${MEDIA_REMOVE_DAYS} --prune-profiles --concurrency=${TOOTCTL_CONCURRENCY}
#compose-run tootctl media remove --days=${MEDIA_REMOVE_DAYS} --prune-headers --concurrency=${TOOTCTL_CONCURRENCY}
# --- DO NOT RUN THESE COMMANDS ---

# Remove orphaned media files (located under public/system) and refresh media files (optional)
compose-run tootctl media remove-orphans
compose-run tootctl media refresh --days=${MEDIA_REFRESH_DAYS} --concurrency=${TOOTCTL_CONCURRENCY}

# This is probably unnecessary
#compose-run tootctl accounts refresh --all

