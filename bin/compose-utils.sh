# --- Compose configuration ---
# COMPOSE_CLI: The compose implementation to use (e.g. docker, podman, nerdctl, etc.)
COMPOSE_CLI=${COMPOSE_CLI:-"docker compose"}

function compose() {
    echo "Running compose command: $@"
    ${COMPOSE_CLI} $@
}
