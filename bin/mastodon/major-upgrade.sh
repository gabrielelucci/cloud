#!/bin/sh

PROJECT_DIR=$(pwd)

# Load the compose-utils.sh script.
cd "$( dirname -- "$0"; )";
source ../compose-utils.sh

# Reset to the appropriate compose project directory.
cd ${PROJECT_DIR}

# --- Script configuration ---
# COMPOSE_SERVICE: The service to run the command on (check compose yaml file)
COMPOSE_SERVICE=${COMPOSE_SERVICE:-"app"}

compose build && compose pull;
compose run --rm -e SKIP_POST_DEPLOYMENT_MIGRATIONS=true ${COMPOSE_SERVICE} bundle exec rails db:migrate && \
compose up -d
compose run --rm ${COMPOSE_SERVICE} bundle exec rails db:migrate
