
# Gabriele Lucci's compose projects

Collection of Containerfiles and compose files used to deploy my services.

## Plans for the future

I'm planning to migrate from a compose-based configuration to [Podman systemd units](https://docs.podman.io/en/latest/markdown/podman-systemd.unit.5.html) (formerly known as quadlets).

I experimented with this kind of configuration on another system, and I thoroughly enjoyed how streamlined the experience is, and I believe it integrates with Linux much better.

---

## Deploy

### Deploy with go-task task runner

If [go-task](https://github.com/go-task/task) is installed, you can invoke the tasks defined for this project to deploy services.

For example, running `go-task deploy-mastodon` will deploy the [`mastodon`](./mastodon/compose.yaml) compose project using `docker compose` CLI.

See the [Taskfile](./Taskfile.yaml) or `task --list-all` for an exhaustive list of available tasks.

## Miscellaneous

### Restore postgres backups

On a project that both has `docker.io/library/postgres:15` and `docker.io/eeshugerman/postgres-backup-s3`, it is possible to execute a manual backup with:

```sh
# "backup" is the name of the service
docker compose exec backup sh backup.sh
```

Similarly, to restore the latest available backup:

```sh
docker compose exec backup sh restore.sh
```

### Upgrade postgres version

Postgres official docker image doesn't support automatic updates to major versions. To achieve that, it is recommended to first do a manual backup.

```sh
# Stop all services:
docker compose stop
# Start just the `db` and `backup` services:
docker compose up -d db backup
# Execute manual backup:
docker compose exec backup sh backup.sh
```

At this point, you stop and remove all the containers of the project, delete the volume containing the postgres data, bump the postgres official image to the newer major version, and then restore the backup.

```sh
# Stop and remove all the containers:
docker compose down
# Delete the volume (make sure you actually have a restorable backup):
docker compose rm "<volume_name>"
# Change the postgres version in the compose.yaml file.
# Recreate the containers (and fresh volume):
docker compose up -d db backup
# Restore the latest backup
docker compose exec backup sh restore.sh
# Recreate the rest of the containers:
docker compose up -d
```
