#!/bin/bash

pwd

source "$(dirname ${0})/../../activate_env.sh"

docker compose up --build -d

cd ../../caddy

echo "
{{ cookiecutter.domain_name }} {
    reverse_proxy {{ cookiecutter.project_slug }}-wordpress-1:80
}
" > sites-enabled/{{ cookiecutter.domain_name }}

docker compose up --build -d
